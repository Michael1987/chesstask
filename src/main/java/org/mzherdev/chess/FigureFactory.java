package org.mzherdev.chess;

import org.mzherdev.chess.model.*;

import java.util.Arrays;
import java.util.List;

public class FigureFactory {

    public static final char KING = 'K';
    public static final char QUEEN = 'Q';
    public static final char ROOK = 'R';
    public static final char BISHOP = 'B';
    public static final char KNIGHT = 'N';
    public static final List<Character> FIGURES = Arrays.asList(KING, QUEEN, ROOK, BISHOP, KNIGHT);

    public static final char EMPTY_CELL = '0';
    public static final char ATTACKED_CELL = '*';
    public static final char INTERSECT_CELL = '!';

    public static Figure create(Character name, int x, int y) {
        Figure figure = null;
        switch (name) {
            case QUEEN:
                figure = new Queen(x, y);
                break;
            case KING:
                figure = new King(x, y);
                break;
            case ROOK:
                figure = new Rook(x, y);
                break;
            case BISHOP:
                figure = new Bishop(x, y);
                break;
            case KNIGHT:
                figure = new Knight(x, y);
                break;
        }
        return figure;
    }
}
