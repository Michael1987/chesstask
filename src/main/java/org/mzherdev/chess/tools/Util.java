package org.mzherdev.chess.tools;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Util {

    private Util() {}

    /**
     * Returns collection of all possible positions of figures on the desk.
     * @param s string, containing figures and empty cells
     * @return <tt>Set</tt> of all possible positions of figures as string.
     */
    //https://stackoverflow.com/questions/4240080/generating-all-permutations-of-a-given-string
    public static Set<String> permutate(String s) {
        Queue<String> permutations = new LinkedList<>();
        Set<String> v = new HashSet<>();
        permutations.add(s);
        while (permutations.size() != 0) {
            String str = permutations.poll();
            if (!v.contains(str)) {
                v.add(str);
                for (int i = 0; i < str.length(); i++) {
                    String c = String.valueOf(str.charAt(i));
                    permutations.add(str.substring(i + 1) + c + str.substring(0, i));
                }
            }
        }
        return v;
    }


    /**
     * Converts array of characters to bidimensional array.
     * @param array converting array
     * @param m length of outer array
     * @param n length of inner array
     * @return bidimensional array
     */
    public static char[][] monoArrayToBidimensional(char[] array, int m, int n) {
        if (array.length != (m * n))
            throw new IllegalArgumentException("Invalid length of array");

        char[][] res = new char[m][n];
        for (int i = 0; i < m; i++)
            System.arraycopy(array, (i * n), res[i], 0, n);
        return res;
    }

    /**
     * Prints the array.
     * @param desk bidimensional array that needed to be printed to console
     */
    public static void printMatrix(char[][] desk) {
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                System.out.print(desk[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
