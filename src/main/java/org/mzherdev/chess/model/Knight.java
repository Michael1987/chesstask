package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.KNIGHT;

public class Knight extends Figure {

    int[][] possibleMovements = {
            {-2, 1},
            {-1, 2},
            {1, 2},
            {2, 1},
            {2, -1},
            {1, -2},
            {-1, -2},
            {-2, -1}
    };

    public Knight(int x, int y) {
        super(x, y);
    }

    @Override
    public void getAttackedFields(char[][] board) {
        for(int[] mov : possibleMovements) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    if(i == getX() + mov[0] && j == getY() + mov[1])
                        board[i][j] = setCellValue(board[i][j]);
                }
            }
        }
        board[getX()][getY()] = KNIGHT;
    }
}
