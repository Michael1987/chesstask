package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.ATTACKED_CELL;
import static org.mzherdev.chess.FigureFactory.FIGURES;
import static org.mzherdev.chess.FigureFactory.INTERSECT_CELL;

public abstract class Figure {

    private final int x;
    private final int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract void getAttackedFields(char[][] board);

    protected char setCellValue(char c) {
        return FIGURES.contains(c) ? INTERSECT_CELL : ATTACKED_CELL;
    }
}
