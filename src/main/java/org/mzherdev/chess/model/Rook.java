package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.ROOK;

public class Rook extends Figure {

    public Rook(int x, int y) {
        super(x, y);
    }

    @Override
    public void getAttackedFields(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if(i == getX() || j == getY())
                    board[i][j] = setCellValue(board[i][j]);
            }
        }
        board[getX()][getY()] = ROOK;
    }
}
