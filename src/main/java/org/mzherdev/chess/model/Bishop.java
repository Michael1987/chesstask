package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.BISHOP;

public class Bishop extends Figure {

    public Bishop(int x, int y) {
        super(x, y);
    }

    @Override
    public void getAttackedFields(char[][] board) {
        //moves in the down positive diagonal
        for (int i = getX() + 1, j = getY() + 1; i < board.length && j < board[i].length; i++, j++) {
            board[i][j] = setCellValue(board[i][j]);
        }
        //moves in the up positive diagonal
        for (int i = getX() + 1, j = getY() - 1; i < board.length && j > -1 && j < board[i].length; i++, j--) {
            board[i][j] = setCellValue(board[i][j]);
        }
        //moves in the up negative diagonal
        for (int i = getX() - 1, j = getY() - 1; j > -1 && i > -1; i--, j--) {
            board[i][j] = setCellValue(board[i][j]);
        }
        //moves in the down negative diagonal
        for (int i = getX() - 1, j = getY() + 1;  i > -1 && i < board.length && j < board[i].length; i--, j++) {
            board[i][j] = setCellValue(board[i][j]);
        }
        board[getX()][getY()] = BISHOP;
    }
}
