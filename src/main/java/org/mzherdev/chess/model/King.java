package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.KING;

public class King extends Figure {

    int[][] possibleMovements = {
            {1, 0},
            {0, 1},
            {-1, 0},
            {0, -1},
            {1, 1},
            {-1, 1},
            {-1, -1},
            {1, -1}
    };

    public King(int x, int y) {
        super(x, y);
    }

    @Override
    public void getAttackedFields(char[][] board) {
        for(int[] mov : possibleMovements) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    if(i == getX() + mov[0] && j == getY() + mov[1])
                        board[i][j] = setCellValue(board[i][j]);
                }
            }
        }
        board[getX()][getY()] = KING;
    }
}
