package org.mzherdev.chess.model;

import static org.mzherdev.chess.FigureFactory.QUEEN;

public class Queen extends Figure {

    private Figure rook;
    private Figure bishop;

    public Queen(int x, int y) {
        super(x, y);
        rook = new Rook(x, y);
        bishop = new Bishop(x, y);
    }

    @Override
    public void getAttackedFields(char[][] board) {
        rook.getAttackedFields(board);
        bishop.getAttackedFields(board);
        board[getX()][getY()] = QUEEN;
    }
}
