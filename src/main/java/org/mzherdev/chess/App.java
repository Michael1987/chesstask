package org.mzherdev.chess;

import java.util.*;
import java.util.stream.Collectors;

import static org.mzherdev.chess.FigureFactory.*;
import static org.mzherdev.chess.tools.Util.*;

/**
 * K - king
 * Q - queen
 * R - rook
 * B - bishop
 * N - knight
 */
public class App {

    public static void main(String[] args) {
        if (args.length < 3)
            throw new IllegalArgumentException("There must be at least one figure on the desk.");

        int m = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        String[] figures = Arrays.copyOfRange(args, 2, args.length);

        List<char[][]> solution = solve(m, n, figures);

        System.out.println("Possible positions: ");
        for (char[][] desk : solution) {
            printMatrix(desk);
        }

        System.out.println("Number of positions: " + solution.size());
    }

    static List<char[][]> solve(int m, int n, final String... figures) {
        List<char[][]> possibleFigurePositions = new ArrayList<>();

        StringBuilder chessCellValues = new StringBuilder();
        for (String f : figures)
            chessCellValues.append(f.toUpperCase());
        while (chessCellValues.length() < m * n)
            chessCellValues.append(EMPTY_CELL);

        Set<String> possibleCombinations = permutate(chessCellValues.toString());
        for (String s : possibleCombinations)
            possibleFigurePositions.add(monoArrayToBidimensional(s.toCharArray(), m, n));

        // check all chess movements
        for (char[][] desk : possibleFigurePositions) {
            for (int i = 0; i < desk.length; i++) {
                for (int j = 0; j < desk[i].length; j++) {
                    char currentCell = desk[i][j];
                    if(FIGURES.contains(desk[i][j]))
                        FigureFactory.create(currentCell, i, j).getAttackedFields(desk);
                    else if (currentCell != INTERSECT_CELL && currentCell != EMPTY_CELL && currentCell != ATTACKED_CELL)
                        throw new IllegalArgumentException(currentCell + " - incorrect figure. Should be K, Q, R, B, N");
                }
            }
        }

        return possibleFigurePositions.stream()
                // filter all that don`t intersects with other figures
                .filter(array -> Arrays.deepToString(array).indexOf(INTERSECT_CELL) < 0)
                // filter all that contains all figures from arguments
                .filter(array -> {
                    int numberOfFiguresOnTheDesk = 0;
                    for (int i = 0; i < array.length; i++) {
                        for (int j = 0; j < array[i].length; j++) {
                            if(FIGURES.contains(array[i][j]))
                                numberOfFiguresOnTheDesk++;
                        }
                    }
                    return numberOfFiguresOnTheDesk == figures.length;
                })
                .collect(Collectors.toList());

    }
}
