package org.mzherdev.chess;

import org.junit.Assert;
import org.junit.Test;
import org.mzherdev.chess.tools.Util;

import java.util.List;

import static org.mzherdev.chess.FigureFactory.*;

public class AppTest {

    @Test
    public void testKings() {
        List<char[][]> result = App.solve(2,2, String.valueOf(KING), String.valueOf(KING));
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testQueen() {
        List<char[][]> result = App.solve(2,2, String.valueOf(QUEEN), String.valueOf(QUEEN));
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void testRooks() {
        List<char[][]> result = App.solve(2,2, String.valueOf(ROOK), String.valueOf(ROOK));
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void testBishops() {
        List<char[][]> result = App.solve(2,2, String.valueOf(BISHOP), String.valueOf(BISHOP));
        result.forEach( r -> Util.printMatrix(r));
        Assert.assertEquals(4, result.size());
    }

    @Test
    public void testKnights() {
        List<char[][]> result = App.solve(2,2, String.valueOf(KNIGHT), String.valueOf(KNIGHT));
        Assert.assertEquals(6, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongFigure() {
        App.solve(2,2, "W");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongFigure2() {
        App.solve(2,2, "www");
    }

}