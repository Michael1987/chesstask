package org.mzherdev.chess;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Warmup(iterations = 10)
@Measurement(iterations = 10)
@BenchmarkMode({Mode.SingleShotTime})
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Threads(1)
@Fork(10)
@Timeout(time = 5, timeUnit = TimeUnit.MINUTES)
public class ChessBenchmark {

    @Param({"4"})
    private int m;

    @Param({"4"})
    private int n;

    private String[] figures;

    @Setup
    public void setUp() {
            figures = new String[]{"k", "n", "q", "r"};

    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(ChessBenchmark.class.getSimpleName())
                .threads(1)
                .forks(10)
                .timeout(TimeValue.minutes(5))
                .build();
        new Runner(options).run();
    }

    @Benchmark
    public List<char[][]> measure() throws Exception {
        return App.solve(m, n, figures);
    }

}
